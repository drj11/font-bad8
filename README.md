# Bad 8 (working title)

This is an 8 x 8 pixel font.
Spacing is extra.
Most of the glyphs use all 8 pixels of width,
so it cannot be used without putting extra space between the
letters.

## Design Notes


### Capital letters

The capital letters are monospaced.
All letters are on a body 8 pixels wide (256 design units).
All except the capital **I** have a printed graphic that is 8 pixels
wide (the **I** is 6).

Vertically, the cap-height is 8 pixels.
The **E** shows a 2:1:2:1:2 rhythm that is respected by the rest of
the alphabet.

The basic stroke thickness is 2 pixels for both horizontal and
vertical.
Obviously in a pixel-based font there is some struggle with the
diagonals.

Curves/corners (**A** **C** **D** **O** **S**) are mostly represented
by a missing pixel, occasionally (in **D**) by a missing L-triomino.

The font is sans serif with terminals mostly blunt cut on the
vertical or horizontal grid.
**C** **G** **M** **S** show some variation in terminal ending and
stroke-joining.


### Little letters

The **o** is 7×7 pixels on a 8-pixel wide body.
It has one pixel of space on the left, meaning that a word can start
with a capital letter and continue in little letters and the spacing
will be reasonable.

7-pixel x-height is very high compared to an 8-pixel cap-height.
This is deliberately experimental.

Compared to the capital dimensions of 8×8, a 7×7 square is slightly
compressed.
For **m** and **e** we have used a rhythm of 2:1:1:1:2 both vertical
(for **e**) and horizontall (for **m**).

3 pixel ascenders and descenders.


## END
